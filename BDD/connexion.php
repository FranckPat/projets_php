<?php
// CLASSES STATIC
class Connexion {
    static function connect() {
        require_once('config/config.php');
        $bdd = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
        return $bdd;
    }
}
?>